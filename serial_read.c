#include <windows.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <conio.h>

/*---------------------*/
/* Compilation options */
/*---------------------*/

/* Generate a valid predefined message instead of receiving messages from the
 * serial port */
//#define TEST_MESSAGE

#define EXIT_KEY                'q'
#define SIZE_READ_BUFFER        255

#ifdef DEBUG
#define PRINT_DEBUG(fmt, ...)   \
    fprintf(stderr, "DEBUG   " fmt "\n", ##__VA_ARGS__)
#else
#define PRINT_DEBUG(fmt, ...)   (0)
#endif

#define PRINT_INFO(fmt, ...)    \
    fprintf(stderr, "INFO    " fmt "\n", ##__VA_ARGS__)

#define PRINT_WARN(fmt, ...)    \
    fprintf(stderr, "WARN    " fmt "\n", ##__VA_ARGS__)

#define PRINT_ERROR(fmt, ...)   \
    fprintf(stderr, "ERROR   %s:%d: " fmt "\n", __func__, __LINE__, ##__VA_ARGS__)

//#define LOG_ERR(fmt, ...)       \
//    fprintf(stderr, "ERROR   %s:%d: error [%s] " fmt "\n", __func__, __LINE__, strerror(errno), ##__VA_ARGS__)

static void check_and_print_available_serial_ports(void)
{
    HANDLE hSerial = NULL;
    fprintf(stderr, "        Available serial ports: ");
    for(int i = 0; i <= 255; i++)
    {
        char buffer[20] = { 0 };
        sprintf(buffer, "COM%d", i);
        hSerial = CreateFile(buffer, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if(hSerial != INVALID_HANDLE_VALUE)
        {
            CloseHandle(hSerial);
            fprintf(stderr, "%s ", buffer);
        }
    }
    fprintf(stderr, "\n");
}

static bool get_first_available_serial_port(char * name)
{
    HANDLE hSerial = NULL;
    fprintf(stderr, "        Searching available serial ports... ");
    for(int i = 0; i <= 255; i++)
    {
        sprintf(name, "COM%d", i);
        hSerial = CreateFile(name, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if(hSerial != INVALID_HANDLE_VALUE)
        {
            CloseHandle(hSerial);
            fprintf(stderr, "%s\n", name);
            return true;
        }
    }
    fprintf(stderr, "\n");
    PRINT_WARN("No serial port found!");
    return false;
}

static HANDLE open_serial_port(char * name)
{
    HANDLE hSerial = NULL;
    hSerial = CreateFile(name, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hSerial == INVALID_HANDLE_VALUE)
    {
        hSerial = NULL;
    }
    return hSerial;
}

static bool set_serial_port_params(HANDLE hSerial, uint32_t bitrate)
{
    DCB dcbSerialParams = { 0 };
    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
    if (GetCommState(hSerial, &dcbSerialParams) == 0)
    {
        return false;
    }

    dcbSerialParams.BaudRate = bitrate;
    dcbSerialParams.ByteSize = 8;
    dcbSerialParams.StopBits = ONESTOPBIT;
    dcbSerialParams.Parity = NOPARITY;
    if(SetCommState(hSerial, &dcbSerialParams) == 0)
    {
        return false;
    }
    return true;
}

static bool set_serial_port_timeouts(HANDLE hSerial)
{
    COMMTIMEOUTS timeouts = { 0 };
    /* Tmax = Ntotal * ReadTotalTimeoutMultiplier + ReadTotalTimeoutConstant */
    /* The maximum amount of time, in milliseconds, that is allowed between two consecutive bytes in a read operation */
    timeouts.ReadIntervalTimeout = 5;
    /* The maximum amount of time, in milliseconds, that is allowed per byte in a read operation */
    timeouts.ReadTotalTimeoutMultiplier = 2;
    /* The maximum amount of additional time, in milliseconds, that is allowed per read operation */
    timeouts.ReadTotalTimeoutConstant = 10;
    /* Tmax = Ntotal * WriteTotalTimeoutMultiplier + WriteTotalTimeoutConstant */
    /* The maximum total time, in milliseconds, that is allowed per byte in a write operation */
    timeouts.WriteTotalTimeoutMultiplier = 10;
    /* The maximum amount of additional time, in milliseconds, that is allowed per write operation */
    timeouts.WriteTotalTimeoutConstant = 10;

    if(SetCommTimeouts(hSerial, &timeouts) == 0)
    {
        return false;
    }
    return true;
}

int main(int argc, char ** argv) {
    HANDLE hSerial = NULL;
    char * port_name;
#ifdef WRITE_SERIAL
    const char ack = 6;
    DWORD bytes_written = 0;
#endif
    DWORD bytes_read = 0;
    BOOL status = FALSE;
    char read_buffer[SIZE_READ_BUFFER] = { 0 };
    BOOL fWaitingOnRead = FALSE;
    
    port_name = (char *)malloc(20);

    /* Check arguments */
#ifndef TEST_MESSAGE
    if (argc < 2)
    {
        
        PRINT_WARN("No serial port specified (e.g COM1)");
        /* no serial port have been given, get first available port */
        if(!get_first_available_serial_port(port_name))
        {
            return EXIT_FAILURE;
        }
    }
    else
    {
        port_name = argv[1];
    }

    /* Open serial port */
    PRINT_INFO("Opening serial port %s...", port_name);
    hSerial = open_serial_port(port_name);
    if(NULL == hSerial)
    {
        free(port_name);
        PRINT_ERROR("Could not open serial port");
        /* invalid serial port, check available serial ports */
        check_and_print_available_serial_ports();
        return EXIT_FAILURE;
    }
    free(port_name);

    if(!set_serial_port_params(hSerial, CBR_115200))
    {
        CloseHandle(hSerial);
        PRINT_ERROR("Could not set device parameters");
        return EXIT_FAILURE;
    }
    
    if(!set_serial_port_timeouts(hSerial))
    {
        PRINT_ERROR("Could not set timeouts");
        CloseHandle(hSerial);
        return EXIT_FAILURE;
    }
#endif

    PRINT_INFO("Press '%c' to quit", EXIT_KEY);
    PRINT_INFO("- - - - - - - - - -");

    while (1)
    {
        /* read data */
        if (!ReadFile(hSerial, &read_buffer, sizeof(read_buffer), &bytes_read, NULL))
        {
            if (GetLastError() != ERROR_IO_PENDING) /* read not delayed? */
            {
                /* Error in communications; report it */
                PRINT_ERROR("Could not read serial port data");
                CloseHandle(hSerial);
                return EXIT_FAILURE;
            }
        }
        else
        {
            PRINT_DEBUG("Number of bytes read: %u", bytes_read);
            /* read completed, process data */
            printf("%s", read_buffer);
        }
        /* check if the keyboard has been hit */
        if (_kbhit())
        {
            /* if the exit key has been pressed, exit the infinite loop */
            if(EXIT_KEY == getch())
            {
                break;
            }
        }
    }
    PRINT_INFO("- - - - - - - - - -");

#ifdef WRITE_SERIAL
    fprintf(stderr, "Sending ACK... ");
    if(!WriteFile(hSerial, &ack, 1, &bytes_written, NULL))
    {
        fprintf(stderr, "Error: Could not send ACK.\n");
        CloseHandle(hSerial);
        return EXIT_FAILURE;
    }
#endif

#ifndef TEST_MESSAGE
    PRINT_INFO("Closing serial port... ");
    if (CloseHandle(hSerial) == 0)
    {
        PRINT_ERROR("Could not close serial port");
        return EXIT_FAILURE;
    }
#endif

    return EXIT_SUCCESS;
}