# Serial receiver

Receive data from the serial port (Windows only)

Usage is: serial_read.exe COM5

If the serial port name is missing, the first available serial port is used.

Compilation: gcc.exe -g -DWIN32 serial_read.c -o serial_read.exe